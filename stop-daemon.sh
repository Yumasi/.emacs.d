#!/bin/bash

server_ok() {
    emacsclient -a "false" -e "(boundp 'server-process)"
}

if [ "t" == "$(server_ok)" ]; then
    echo "Shutting down Emacs server"
    emacsclient -e '(kill-emacs)'
else
    echo "Emacs server not running"
fi
